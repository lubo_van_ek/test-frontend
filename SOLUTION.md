# SOLUTION

## Estimation

Estimated: 4 hours

Spent: 2.5 hours

## Solution

### The product will be split up into these parts:

- `App Component` - component holds the application data and is responsible for fetching and updating it
- `Url Builder Service` - service that creates URLs used for fetching data
- `Products Component` - parent component that holds a list of `Product` components and is also reponsible for displaying Product Layout
- `Product Component` - component responsible for displaying individual products
- `Filters Component` - component reponsible for changing state of App Component which will trigger re-render on a button press
- `Pagination Component` - component responsible for updating the current page state

### Tasks to do:

- Add typescript - for type checking
- Setup ESLint and Prettier - for linting and code formatting
- Add styling libraries
- Crete individual components from above
- Write tests (not part of this solution)

### To save time:

- I will be using Material UI and related libraries to create a more pleasant experience to look at
- I won't be using Storybook to develop individual components in isolation
- I won't be writting Unit tests as for the application of these capabilities there is not much to test
- I won't be writing integration tests as I don't have much experience in doing so and I always had a QA person at hand who can do that
- I will be commiting straight to main branch rather than creating a feature branch for every part of the application (as there is only me commiting the code)
- I won't be handling any data validation, server not being available etc.
- I won't be handling data Loading and indication of this state on the applicaiton
- I won't be handling data caching to save the number which would save the number of calls to the API

### API side

- Product `id` hat to be unique otherwise it doesn't provude much value. I've found there is `id: 9` twice and `id: 11` is missing which I've decided to fix. The id will most likely be used as a key for a Product Component (and would cause errors). We could concatanate for example an `id` and a `name` to get this information. However, this would be only a workaround for the actual issue.
- The API doesn't return number of pages, or number of products which would be the preferred way of working with pagination. Ideal solution would be to enhance the API with this information. I've decided to use read the `X-Total-Count` header from the API to obtain this information.

### Test cases:

- using a price filter with `29.95` returns 1 product
- using a price filter with `29.94` doesn't return any products
- only numerical values are available to put in the price filter
- using a tag filter with `Tiger` doesn't return any products
- subscription filter has 3 values: `not-selected`, `Yes` and `No`
- using a subcription filter with `Yes` returns 7 products
- using a subcription filter with `No` returns 5 products
- pagination resets to the first page with every filter call

### Caveat

I am not sure if json-server can search for a value based on multiple tags where the order doesn't matter. That's the only reason I didn't use a multi-select for Tags filter.

### Better Product

To make a better product all the things that were ommited in `To save time section` could be done. Also not using a Material-UI library and writing styles either from scratch or using Tailwind.css would make the bundle leaner. My estimate for doing all these steps is 20-40 hours.

### Improving API

A Graph-QL api could be used instead of REST so not every piece of data that is not used is downloaded to the client machine.
