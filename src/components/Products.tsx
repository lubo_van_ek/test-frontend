import { Grid } from "@mui/material";
import { ProductType } from "../types/ProductType";
import Product from "./Product";

const Products: React.FC<{ products: ProductType[] }> = ({ products }) => (
  <Grid container spacing={2} padding={2}>
    {products.map((product) => (
      <Product key={product.id} product={product} />
    ))}
  </Grid>
);

export default Products;
