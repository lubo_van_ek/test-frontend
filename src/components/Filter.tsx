import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import Grid from "@mui/material/Grid/Grid";
import React, { Dispatch, SetStateAction, useState } from "react";

const Filter: React.FC<{
  setTags: Dispatch<SetStateAction<string>>;
  setPrice: Dispatch<SetStateAction<string>>;
  setSubscription: Dispatch<SetStateAction<string>>;
  resetPage: () => void;
}> = ({ setTags, setPrice, setSubscription, resetPage }) => {
  const [tagFilter, setTagFilter] = useState("");
  const [subscriptionFilter, setSubscriptionFilter] = useState("");
  const [priceFilter, setPriceFilter] = useState("");
  const handleSubmit = (e: React.SyntheticEvent): void => {
    setTags(tagFilter);
    setPrice(priceFilter);
    setSubscription(subscriptionFilter);
    resetPage();
    e.preventDefault();
  };
  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={2} padding={2}>
        <Grid item xs={12} sm={6} md={3}>
          <TextField
            fullWidth
            label="Tags"
            type="text"
            value={tagFilter}
            onChange={(e) => setTagFilter(e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <TextField
            fullWidth
            label="Price"
            type="number"
            InputProps={{ inputProps: { min: 0, step: ".01" } }}
            value={priceFilter}
            onChange={(e) => setPriceFilter(e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <FormControl fullWidth>
            <InputLabel id="subscription-label">Subscription</InputLabel>
            <Select
              labelId="subscription-label"
              label="Subscription"
              value={subscriptionFilter}
              onChange={(e) => setSubscriptionFilter(e.target.value)}
            >
              <MenuItem value="">Not Selected</MenuItem>
              <MenuItem value="true">Yes</MenuItem>
              <MenuItem value="false">No</MenuItem>
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <Button
            type="submit"
            variant="outlined"
            fullWidth
            sx={{ height: "100%" }}
          >
            Filter
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Filter;
