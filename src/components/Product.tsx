import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Grid,
  Typography,
} from "@mui/material";
import { ProductType } from "../types/ProductType";

const Product: React.FC<{ product: ProductType }> = ({ product }) => (
  <Grid item xs={12} sm={6} md={4} lg={3}>
    <Card variant="outlined">
      <CardHeader title={product.title} />
      <CardMedia
        component="img"
        image={product.image_src}
        height="200"
        alt={`${product.title} image`}
      />
      <CardContent>
        <Typography align="right">Price: £ {product.price}</Typography>
      </CardContent>
    </Card>
  </Grid>
);

export default Product;
