import { Pagination } from "@mui/material";
import { useEffect, useState } from "react";
import Filter from "./components/Filter";
import Products from "./components/Products";
import { buildUrl } from "./utilities/UrlBuilderService";

const App: React.FC = () => {
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [tags, setTags] = useState("");
  const [price, setPrice] = useState("");
  const [subscription, setSubscription] = useState("");

  const resetPage = (): void => {
    setPage(1);
  };

  useEffect(() => {
    async function fetchData(): Promise<void> {
      const res = await fetch(buildUrl(page, tags, price, subscription));
      const totalCount = res.headers.get("X-Total-Count") || "1";
      setTotalPages(Math.ceil(parseInt(totalCount, 10) / 10.0));
      const data = await res.json();
      setProducts(data);
    }

    fetchData();
  }, [page, tags, price, subscription]);

  return (
    <div className="App">
      <Filter
        setTags={setTags}
        setPrice={setPrice}
        setSubscription={setSubscription}
        resetPage={resetPage}
      />
      <Pagination
        count={totalPages}
        page={page}
        onChange={(_, p) => setPage(p)}
      />
      <Products products={products} />
    </div>
  );
};

export default App;
