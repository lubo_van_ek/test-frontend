import { ParamsType } from "../types/ParamsType";

export const buildUrl = (
  page: number,
  tags: string,
  price: string,
  subscription: string
): string => {
  const url = new URL("http://localhost:3010/products");
  const params: ParamsType = { _page: page.toString() };
  if (tags !== "") {
    params.tags_like = tags;
  }
  if (subscription !== "") {
    params.subscription = subscription;
  }
  if (price !== "") {
    params.price_lte = price;
  }
  url.search = new URLSearchParams(params).toString();
  return url.toString();
};
