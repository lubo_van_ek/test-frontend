/* eslint-disable camelcase */
export type ProductType = {
  id: string;
  title: string;
  image_src: string;
  price: string;
};
