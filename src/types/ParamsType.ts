/* eslint-disable camelcase */
export type ParamsType = {
  _page?: string;
  tags_like?: string;
  price_lte?: string;
  subscription?: string;
};
